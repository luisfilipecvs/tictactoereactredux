class Game{
    constructor() {
      this.p1 = "x";
      this.p2 = "o"
      this.empty = "";
      this.board = [];
      this.initialize_board();
      this.text = "Click on a cell to play";
      this.title = "player 1 turn"; 
      this.move = 1;
      this.winner = 0;
    }
    initialize_board() {
        this.board.push([this.empty,this.empty,this.empty]);
        this.board.push([this.empty,this.empty,this.empty]);
        this.board.push([this.empty,this.empty,this.empty]);
    }
    play(x,y){
        
      let value = this.p1;
      if(this.move % 2 === 0){
        value = this.p2;
      }
      this.board[x][y] = value;
      return this.checkGameState(value);
    }
    checkGameState(value){
        if(this.board[0][0] === value && this.board[0][1] === value && this.board[0][2] === value){
            return this.gameEnd(value);
        }
        if(this.board[1][0] === value && this.board[1][1] === value && this.board[1][2] === value){
            return this.gameEnd(value);
        }
        if(this.board[2][0] === value && this.board[2][1] === value && this.board[2][2] === value){
            return this.gameEnd(value);
        }
        if(this.board[0][0] === value && this.board[1][1] === value && this.board[2][2] === value){
            return this.gameEnd(value);
        }
        if(this.board[0][2] === value && this.board[1][1] === value && this.board[2][0] === value){
            return this.gameEnd(value);
        }
        if(this.board[0][0] === value && this.board[1][0] === value && this.board[2][0] === value){
            return this.gameEnd(value);
        }
        if(this.board[0][1] === value && this.board[1][1] === value && this.board[2][1] === value){
            return this.gameEnd(value);
        }
        if(this.board[0][2] === value && this.board[1][2] === value && this.board[2][2] === value){
            return this.gameEnd(value);
        }
        return this.nextMove();
    }
    nextMove(){
        this.move++;
        if(this.move === 10){
            this.title = "It's a DRAW";
            return true;
        }
        this.title = "player 1 turn";
        if(this.move % 2 === 0){
            this.title = "player 2 turn";
        }
        return false;
    }
    gameEnd (value) {
        this.winner = 1;
        if(this.move % 2 === 0){
            this.winner = 2;
        }
        this.title = "Player "+this.winner+" is the winner";
        this.text = "";
        return true;
    }
}

export default Game