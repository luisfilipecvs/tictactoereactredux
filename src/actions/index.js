
export const startGame = () =>{ 
  return(
  {
    type: 'GAME_START'
  }
  )
}

export const gameplay = (game_ended) =>{ 
  if(game_ended){
    return(
      {
        type: 'GAME_FINISHED'
      }
    )
  }else{
    return(
      {
        type: 'GAME_PLAY'
      }
    )  
  }
  
}