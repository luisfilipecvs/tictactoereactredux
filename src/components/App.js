import React from 'react'
import Footer from './Footer'
import MessageBoard from './MessageBoard'

const App = () => {
  return (

  <div id="body-container">
    <div className="flex-container-center-space-around">
      <MessageBoard />
    </div>
    <Footer />
  </div>
)}

export default App
