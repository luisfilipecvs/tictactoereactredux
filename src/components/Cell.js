import React from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { gameplay } from '../actions'


const mapStateToProps = function(state){
    return {
        title: state.game.title,
        text: state.game.text,
        game: state.game    
    };
}
const mapDispatchToProps = function (dispatch) {
    return bindActionCreators({
      gameplay: gameplay,
    }, dispatch)
}

class Cell extends React.Component {
    playHandler = () => {
        if(this.props.value === "" && this.props.game.winner === 0){
            let ended = this.props.game.play(this.props.x,this.props.y);
            this.props.gameplay(ended);
        }else if(this.props.game.winner === 0){
            alert("please click an empty cell");
        }
    }
     
    render() {
        let icon = null; 
        switch(this.props.value){
            case "o":
                icon = <i className="fa fa-circle-o circle"></i>
                break;
            case "x":
                icon = <i className="fa fa-check check"></i> 
                break;
            default:
                icon = "";
                break;
        }
        
        return (
            <div key={this.props.x+"-"+this.props.y} onClick={this.playHandler}  className="game-cell text-center">
                {icon}
            </div>
        )
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Cell)