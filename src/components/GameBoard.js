import React from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { gameplay } from '../actions'
import Cell  from './Cell'

const mapStateToProps = function(state){
    return {
            title: state.game.title,
            text: state.game.text,
            game : state.game    
        };
}
const mapDispatchToProps = function (dispatch) {
    return bindActionCreators({
      gameplay: gameplay,
    }, dispatch)
  }

class GameBoard extends React.Component {

    render() {
        const cells = this.props.game.board.map( (l, x_index) => {
            return l.map( (c, y_index) => {
                return <Cell x={x_index} y={y_index} value={c} />
            });
        });

        return (
            <div className="playing-board">
                {cells} 
            </div>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(GameBoard)