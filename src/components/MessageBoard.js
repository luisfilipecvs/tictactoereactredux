import React from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { startGame } from '../actions'
import GameBoard from './GameBoard'


const mapStateToProps = function(state){
    return {
            type: state.type, 
            text : state.text,
            title : state.title,
            game: state.game
            };
}
const mapDispatchToProps = function (dispatch) {
    return bindActionCreators({
      startGame: startGame,
    }, dispatch)
  }

class MessageBoard extends React.Component {

    render() {
        const state_type = this.props.type;
        let button = null;
        let board = null;
        if (state_type === "WELCOME_MESSAGE") {
            button = <button type="button" className="btn btn-info" onClick={this.props.startGame}>Start playing tic-tac-toe</button>;
            board = "";
        } else if(state_type === "END_MESSAGE")  {
            button = <button type="button" className="btn btn-info" onClick={this.props.startGame}>Restart the game</button>;
            board = <GameBoard />;
        }else{
            button = "";
            board = <GameBoard />;
        }
        return (
            <div className="game-board">
                <h3 className="text-center">{this.props.title}</h3>
                <p>{this.props.text}</p>
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {board}
                </div>
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div className="text-center">
                        {button}
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(MessageBoard)