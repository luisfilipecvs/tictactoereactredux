import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import App from './components/App'
import reducer from './reducers'
import './assets'
import Background from './assets/images/lightning.jpg';

let appStyle = {
  width: "100%",
  height: "100%",
  backgroundImage: "url(" + Background + ")"
}

const store = createStore(reducer)

render(
  <Provider store={store} >
    <div style={ appStyle }>
      <App />
    </div>
  </Provider>,
  document.getElementById('root')
)
