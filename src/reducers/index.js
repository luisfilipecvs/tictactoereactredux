

import Game from '../Classes/Game.js';

const gamePlay = (state = [], action) => {
  switch (action.type) {
    case 'GAME_FINISHED':
      return{
        type : "END_MESSAGE",
        title: state.game.title,
        text: "",
        game: state.game
      }
    case 'GAME_PLAY':
      return {
        type : "GAME",
        title: state.game.title,
        text: state.game.text,
        game: state.game
      }
    case 'GAME_START':
    return {
          type : "GAME",
          title: "player 1 turn",
          text: "Click on a cell to play",
          game: new Game()
        }
    default:
      state.type = "WELCOME_MESSAGE";
      state.title = "Hello inspired people and react lovers";
      state.text = "This is my first react app. Hope you enjoy it";
      return state;
    }
}

export default gamePlay
